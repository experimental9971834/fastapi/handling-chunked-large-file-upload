"""FastAPI example."""
import asyncio
import os
import random

from fastapi import FastAPI, File, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse

app = FastAPI()

origins = [
    "http://localhost:3000",            # For React dev server
    "http://localhost:5173",            # For Vite dev server
    "http://localhost:4173",            # For Vite production server
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class ResponseContext:
    """Response context."""

    def __init__(self) -> None:
        self.data = None
        self.is_success = False
        self.error_message: str | None = None


@app.get("/")
def read_root():
    """Read root."""
    return {"Hello": "World"}


@app.post("/upload-chunks/")
async def upload_chunks(id: str, file_name: str, file: UploadFile = File(...)):
    """Upload chunks."""
    print(f"Uploading chunk {id} for {file_name}")
    print(f"File size: {file.size}")
    print(f"File Name: {file.filename}")
    response_context = ResponseContext()

    try:
        chunk_path = f"temp/{id}_{file_name}"
        with open(chunk_path, "wb") as buffer:
            buffer.write(await file.read())
            response_context.is_success = True
    except Exception as ex:
        response_context.error_message = str(ex)
        response_context.is_success = False

    # Sleep for random time to simulate network latency
    await asyncio.sleep(random.randint(1, 5))

    return JSONResponse(content=response_context.__dict__)


@app.post('/upload-complete/')
async def upload_complete(file_name: str):
    """Upload complete."""
    response_context = ResponseContext()

    try:
        temp_path = "temp/"
        new_path = f"files/{file_name}"
        chunk_files = sorted(
            [file for file in os.listdir(
                temp_path) if file.endswith(file_name)],
            key=lambda x: int(x.split('_')[0])
        )

        with open(new_path, 'wb') as buffer:
            for file in chunk_files:
                chunk_path = os.path.join(temp_path, file)
                with open(chunk_path, 'rb') as chunk_buffer:
                    buffer.write(chunk_buffer.read())
                os.remove(chunk_path)
        response_context.is_success = True
    except Exception as ex:
        response_context.error_message = str(ex)
        response_context.is_success = False

    return JSONResponse(content=response_context.__dict__)
